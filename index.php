<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S04 Activity:Access Modifiers and Encapsulation</title>
</head>
<body>
	<h1>Building</h1>

	
	<p>The name of the building is <?php echo $building->getName(); ?>.</p>


	<p>The <?php echo $building->getName(); ?> has <?php echo $building->getFloors(); ?> floors.</p>

	<p>The <?php echo $building->getName(); ?> is located at <?php echo $building->getAddress(); ?>.</p>

	<?php $building->setName('Caswynn Complex') ?>

	<p>The name of the building has changed to  <?php echo $building->getName(); ?>.</p>

	<h1>Condominium</h1>

	<p>The name of the building is <?php echo $condominium->getName(); ?>.</p>

	<p>The <?php echo $condominium->getName(); ?> has <?php echo $condominium->getFloors(); ?> floors.</p>

	<p>The <?php echo $condominium->getName(); ?> is located at <?php echo $condominium->getAddress(); ?>.</p>

	<?php $condominium->setName('Enzo Tower') ?>

	<p>The name of the building has changed to  <?php echo $condominium->getName(); ?>.</p>

	

	
</body>
</html>